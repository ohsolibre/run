CC = cc
CFLAGS = -Wall -Wextra -Os
LDFLAGS = -s -lcrypt

run: run.c
	$(CC) $^ -o $@ $(CFLAGS) $(LDFLAGS) 
clean:
	rm -f run
