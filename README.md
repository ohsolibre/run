About
-----

run is sudo alternative. It is designed to be as simple as possible.
It allows to execute program as different user and also set
some capabilities. Everything is controlled through signle config file.
See run.conf for details.

To work properly, run must be owned by root and have SUID bit set.


WARNING
=======
This program is vulnerable to tty pushback attack
(See https://www.halfdog.net/Security/2012/TtyPushbackPrivilegeEscalation)


Vulnerability can be fixed by patching kernel and completely
disabling TIOCSTI functionality.
(example patch https://www.github.com/anthraxx/linux-hardened/commit/70d9a407e85d6e6648e30e058561be77f19fd55d)
