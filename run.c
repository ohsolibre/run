#define _GNU_SOURCE
#include <err.h>
#include <grp.h>
#include <pwd.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <shadow.h>
#include <termios.h>
#include <linux/capability.h>
#include <linux/limits.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define ARRAY_SIZE(x)         (sizeof(x) / sizeof((x)[0]))
#define CAP_TO_MASK64(x)      (1LLU << ((x) & 63))

#define CONF_DELIM	":"
#define CONF_SUBDELIM	","
#define CONF_FILE       "/etc/run.conf"

struct conf {
	FILE *file;
	char *line;
	size_t len;

	char *path;		/* full path to program */
	char *users;		/* users that allowed to run this */
	char *identity;		/* run program as this user */
	char *caps;		/* capabilities list */
	char *flags;		/* e.g. p/n/e */
};

static int cap_from_name(const char *name)
{
	unsigned i;
	const char *caps[] = {
		"chown",            "dac_override",     "dac_read_search",
		"fowner",           "fsetid",           "kill",
		"setgid",           "setuid",           "setpcap",
		"linux_immutable",  "net_bind_service", "net_broadcast",
		"net_admin",        "net_raw",          "ipc_lock",
		"ipc_owner",        "sys_module",       "sys_rawio",
		"sys_chroot",       "sys_ptrace",       "sys_pacct",
		"sys_admin",        "sys_boot",         "sys_nice",
		"sys_resource",     "sys_time",         "sys_tty_config",
		"mknod",            "lease",            "audit_write",
		"audit_control",    "setfcap",          "mac_override",
		"mac_admin",        "syslog",           "wake_alarm",
		"block_suspend",    "audit_read"
	};

	for (i = 0; i < ARRAY_SIZE(caps); i++)
		if (!strcmp(caps[i], name))
			return i;
	errx(1, "unknown capability: %s", name);
}

int capset(cap_user_header_t hdrp, const cap_user_data_t datap);
static void setup_caps(__u64 allowed)
{
	struct __user_cap_header_struct header;
	struct __user_cap_data_struct data[2] = {0};
	unsigned i;
	uid_t uid = getuid();
	header.pid = getpid();
	header.version = _LINUX_CAPABILITY_VERSION_3;

	data[0].permitted = allowed;
	data[1].permitted = allowed >> 32;

	/* We need inheritable only to set ambient */
	data[0].inheritable = uid ? allowed : 0;
	data[1].inheritable = uid ? allowed >> 32 : 0;

	if (capset(&header, data))
		err(1, "can't set capabilities");

	/* Ambient are required only for non-root user. See man 7 capabilities */
	if (uid == 0)
		return;

	for (i = 0; i < CAP_LAST_CAP + 1; i++) {
		if (!(allowed & CAP_TO_MASK64(i)))
			continue;
		if (prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_RAISE, i, 0, 0))
			err(1, "can't set ambient capabilities");
	}
}

static __u64 parse_caps(char *str)
{
	char act;
	const char *s = NULL;
	__u64 mask, ret = 0;

	if (*str == '\0')
		return 0;

	while ((s = strsep(&str, CONF_SUBDELIM))) {
		act = *s;
		if (act == '+' || act == '-')
			s++;

		if (!strcmp(s, "all"))
			/* Select all possible capabilities */
			mask = -1LLU >> (63 - CAP_LAST_CAP);
		else
			mask = CAP_TO_MASK64(cap_from_name(s));

		if (act == '-')
			ret &= ~mask;
		else
			ret |= mask;
	}
	return ret;
}

static void read_pass(char *buf, size_t size)
{
	ssize_t sz;
	struct termios newtios, oldtios;

	tcgetattr(0, &oldtios);
	newtios = oldtios;
	newtios.c_lflag &= ~(ECHO | ISIG);

	tcsetattr(0, TCSAFLUSH, &newtios);
	write(1, "Password: ", 10);
	sz = read(0, buf, size-1);
	write(1, "\n", 1);
	tcsetattr(0, TCSAFLUSH, &oldtios);

	if (sz > 0) {
		if (buf[sz-1] == '\n')
			--sz;
		buf[sz] = '\0';
	} else
		buf[0] = '\0';
}

static void check_pass(const char *name)
{
	struct spwd *s;
	char *hash, pass[256];

	if ((s = getspnam(name)) == NULL || *s->sp_pwdp != '$')
		errx(1, "can't get password entry for %s", name);

	read_pass(pass, sizeof(pass));
	hash = crypt(pass, s->sp_pwdp);
	explicit_bzero(pass, sizeof(pass));

	if (hash == NULL || strcmp(hash, s->sp_pwdp) != 0)
		errx(1, "incorrect password");
}

static void reset_env(const struct passwd *pw)
{
	if (clearenv())
		err(1, "clearenv");

	setenv("HOME", pw->pw_dir, 1);
	setenv("SHELL", pw->pw_shell, 1);
	setenv("PATH", "/bin/:/sbin/:/usr/bin/:/usr/sbin/", 1);

	if (chdir(pw->pw_dir) && chdir("/"))
		err(1, "chdir");
}

static struct passwd *run_as(struct conf *c, const char *usr)
{
	const char *s;
	static struct passwd *self = NULL;

	if (!self && (self = getpwuid(getuid())) == NULL)
		errx(1, "no passwd entry for self");

	if (*c->users) {
		while ((s = strsep(&c->users, CONF_SUBDELIM)) != NULL)
			if (!strcmp(self->pw_name, s))
				break;
		if (!s)
			return NULL;
	}

	if (*c->identity) {
		while ((s = strsep(&c->identity, CONF_SUBDELIM)) != NULL)
			if (!usr || !strcmp(usr, s))
				return getpwnam(s);
		return NULL;
	}

	if (usr && strcmp(usr, self->pw_name))
		return NULL;

	return self;
}

static int config_line(struct conf *c)
{
	ssize_t len;
	for (;;) {
		if ((len = getline(&c->line, &c->len, c->file)) <= 0)
			return -1;
		if (*c->line != '#' && *c->line != '\n')
			break;
	}
	c->line[len-1] = '\0'; 

	c->path     = strsep(&c->line, CONF_DELIM);
	c->users    = strsep(&c->line, CONF_DELIM);
	c->identity = strsep(&c->line, CONF_DELIM);
	c->caps     = strsep(&c->line, CONF_DELIM);
	c->flags    = strsep(&c->line, CONF_DELIM);

	/* Last field will be always NULL if one or more absent */
	if (!c->flags)
		errx(1, "%s: wrong number of fields", c->path);
	return 0;
}

static int file_is_executable(const char *name)
{
	struct stat s;
	return(!stat(name, &s) && (s.st_mode & S_IFREG) &&
	      (s.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH)));
}

static void get_executable_path(const char *name, char *buf)
{
	char *env, *path, exe[PATH_MAX];

	/* Absolute or relative path? */
	if (*name == '/' || *name == '.') {
		if (file_is_executable(name) && realpath(name, buf))
			return;
		goto fail;
	}

	/* Find executable in PATH */
	env = getenv("PATH");
	while ((path = strsep(&env, ":")) != NULL) {
		strlcpy(exe, path, sizeof(exe));
		strlcat(exe, "/", sizeof(exe));
		strlcat(exe, name, sizeof(exe));

		/* Leave PATH unmodified */
		if (env)
			*(env-1) = ':';

		if (file_is_executable(exe) && realpath(exe, buf))
			return;
	}
fail:
	errx(1, "%s: file not found or is not an executable", name);
}

static void usage(void)
{
	extern char *__progname;
	errx(1, "usage: %s [-u USER] PROG [ARG]", __progname);
}

int main(int argc, char **argv)
{
	int opt;
	char path[PATH_MAX], *usr = NULL;
	struct conf c = {0};
	struct passwd *pw;

	while ((opt = getopt(argc, argv, "u:")) != -1) {
		switch (opt) {
		case 'u':
			usr = optarg;
			break;
		default:
			usage();
		}
	}

	if (optind >= argc)
		usage();

	get_executable_path(argv[optind], path);

	if ((c.file = fopen(CONF_FILE, "r")) == NULL)
		err(1, "fopen " CONF_FILE);

	for (;;) {
		if (config_line(&c))
			errx(1, "You are not allowed to run this program");
		if (!strcmp(c.path, path) && (pw = run_as(&c, usr)))
			break;
	}
	fclose(c.file);

	if (!strchr(c.flags, 'e'))
		reset_env(pw);
	if (strchr(c.flags, 'p'))
		check_pass(pw->pw_name);
	if (!strchr(c.flags, 'n') && prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0))
		err(1, "prctl(PR_SET_NO_NEW_PRIVS)");

	if (prctl(PR_SET_KEEPCAPS, 1, 0, 0, 0))
		err(1, "prctl(PR_SET_KEEPCAPS)");
	if (initgroups(pw->pw_name, pw->pw_gid))
		err(1, "initgroups");
	if (setgid(pw->pw_gid))
		err(1, "setgid");
	if (setuid(pw->pw_uid))
		err(1, "setuid");
	setup_caps(parse_caps(c.caps));

	free(c.line);
	execv(path, &argv[optind]);
	err(1, "execve");
}
